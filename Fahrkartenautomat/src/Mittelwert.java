import java.util.Scanner;

public class Mittelwert {

	public static void main(String[] args) {

		// (E) "Eingabe"
		// Werte für x und y festlegen:
		// ===========================

		double x = liesDoublewertEin("Bitte Zahl 1 eingeben: ");
		double y = liesDoublewertEin("bitte Zahl 2 eingeben : ");

		double m;

		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		m = mittelwertBerechnen(x, y);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
	}

	public static double liesDoublewertEin(String frage) {
		double zahl;
		Scanner scan = new Scanner(System.in);
		System.out.println(frage);
		zahl = scan.nextDouble();
		return zahl;
	}

	public static double mittelwertBerechnen(double x, double y) {
		double m = (x + y) / 2.0;
		return m;

	}
}

